﻿namespace Steganotastic
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.tabCont = new System.Windows.Forms.TabControl();
            this.encryptionPage = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.psnrTxt = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.enMaxSize = new System.Windows.Forms.Label();
            this.enFileSize = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.psnrButt = new System.Windows.Forms.Button();
            this.enClearAllButt = new System.Windows.Forms.Button();
            this.enWarnMsg = new System.Windows.Forms.Label();
            this.enOptionInpt = new System.Windows.Forms.ComboBox();
            this.enClearButt = new System.Windows.Forms.Button();
            this.encryptButt = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.enKeyInpt = new System.Windows.Forms.TextBox();
            this.enBrowseButt = new System.Windows.Forms.Button();
            this.enFileInpt = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.enProgBar = new System.Windows.Forms.ProgressBar();
            this.decryptionPage = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.deOriginal = new System.Windows.Forms.CheckBox();
            this.deWarnMsg = new System.Windows.Forms.Label();
            this.deClearAll = new System.Windows.Forms.Button();
            this.deOptionInpt = new System.Windows.Forms.ComboBox();
            this.deClearButt = new System.Windows.Forms.Button();
            this.decryptButt = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.deKeyInpt = new System.Windows.Forms.TextBox();
            this.deBrowseButt = new System.Windows.Forms.Button();
            this.deFileInpt = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.deProgBar = new System.Windows.Forms.ProgressBar();
            this.helpPage = new System.Windows.Forms.TabPage();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.aboutPage = new System.Windows.Forms.TabPage();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.openImage = new System.Windows.Forms.OpenFileDialog();
            this.saveImage = new System.Windows.Forms.SaveFileDialog();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.rawPreview = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.rawSave = new System.Windows.Forms.Button();
            this.rawPic = new System.Windows.Forms.PictureBox();
            this.steganoPreview = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.steganoSave = new System.Windows.Forms.Button();
            this.steganoPic = new System.Windows.Forms.PictureBox();
            this.openFile = new System.Windows.Forms.OpenFileDialog();
            this.saveFile = new System.Windows.Forms.SaveFileDialog();
            this.tabCont.SuspendLayout();
            this.encryptionPage.SuspendLayout();
            this.panel1.SuspendLayout();
            this.decryptionPage.SuspendLayout();
            this.panel2.SuspendLayout();
            this.helpPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.aboutPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rawPic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.steganoPic)).BeginInit();
            this.SuspendLayout();
            // 
            // tabCont
            // 
            this.tabCont.Controls.Add(this.encryptionPage);
            this.tabCont.Controls.Add(this.decryptionPage);
            this.tabCont.Controls.Add(this.helpPage);
            this.tabCont.Controls.Add(this.aboutPage);
            this.tabCont.Location = new System.Drawing.Point(12, 305);
            this.tabCont.Name = "tabCont";
            this.tabCont.SelectedIndex = 0;
            this.tabCont.Size = new System.Drawing.Size(740, 209);
            this.tabCont.TabIndex = 0;
            // 
            // encryptionPage
            // 
            this.encryptionPage.Controls.Add(this.panel1);
            this.encryptionPage.Location = new System.Drawing.Point(4, 22);
            this.encryptionPage.Name = "encryptionPage";
            this.encryptionPage.Padding = new System.Windows.Forms.Padding(3);
            this.encryptionPage.Size = new System.Drawing.Size(732, 183);
            this.encryptionPage.TabIndex = 0;
            this.encryptionPage.Text = "Encryption";
            this.encryptionPage.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.psnrTxt);
            this.panel1.Controls.Add(this.label27);
            this.panel1.Controls.Add(this.enMaxSize);
            this.panel1.Controls.Add(this.enFileSize);
            this.panel1.Controls.Add(this.label25);
            this.panel1.Controls.Add(this.psnrButt);
            this.panel1.Controls.Add(this.enClearAllButt);
            this.panel1.Controls.Add(this.enWarnMsg);
            this.panel1.Controls.Add(this.enOptionInpt);
            this.panel1.Controls.Add(this.enClearButt);
            this.panel1.Controls.Add(this.encryptButt);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.enKeyInpt);
            this.panel1.Controls.Add(this.enBrowseButt);
            this.panel1.Controls.Add(this.enFileInpt);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.enProgBar);
            this.panel1.Location = new System.Drawing.Point(6, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(720, 173);
            this.panel1.TabIndex = 2;
            // 
            // psnrTxt
            // 
            this.psnrTxt.AutoSize = true;
            this.psnrTxt.Location = new System.Drawing.Point(363, 149);
            this.psnrTxt.Name = "psnrTxt";
            this.psnrTxt.Size = new System.Drawing.Size(40, 13);
            this.psnrTxt.TabIndex = 18;
            this.psnrTxt.Text = "= 0 DB";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(450, 65);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(53, 13);
            this.label27.TabIndex = 17;
            this.label27.Text = "Max Size:";
            // 
            // enMaxSize
            // 
            this.enMaxSize.AutoSize = true;
            this.enMaxSize.Location = new System.Drawing.Point(508, 66);
            this.enMaxSize.Name = "enMaxSize";
            this.enMaxSize.Size = new System.Drawing.Size(30, 13);
            this.enMaxSize.TabIndex = 16;
            this.enMaxSize.Text = "0 KB";
            // 
            // enFileSize
            // 
            this.enFileSize.AutoSize = true;
            this.enFileSize.Location = new System.Drawing.Point(508, 44);
            this.enFileSize.Name = "enFileSize";
            this.enFileSize.Size = new System.Drawing.Size(30, 13);
            this.enFileSize.TabIndex = 16;
            this.enFileSize.Text = "0 KB";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(450, 43);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(55, 13);
            this.label25.TabIndex = 15;
            this.label25.Text = "File Size : ";
            // 
            // psnrButt
            // 
            this.psnrButt.Enabled = false;
            this.psnrButt.Location = new System.Drawing.Point(281, 144);
            this.psnrButt.Name = "psnrButt";
            this.psnrButt.Size = new System.Drawing.Size(75, 23);
            this.psnrButt.TabIndex = 7;
            this.psnrButt.Text = "PSNR";
            this.psnrButt.UseVisualStyleBackColor = true;
            this.psnrButt.Click += new System.EventHandler(this.psnrButt_Click);
            // 
            // enClearAllButt
            // 
            this.enClearAllButt.Location = new System.Drawing.Point(200, 144);
            this.enClearAllButt.Name = "enClearAllButt";
            this.enClearAllButt.Size = new System.Drawing.Size(75, 23);
            this.enClearAllButt.TabIndex = 6;
            this.enClearAllButt.Text = "Clear All";
            this.enClearAllButt.UseVisualStyleBackColor = true;
            this.enClearAllButt.Click += new System.EventHandler(this.ClearAllButt_Click);
            // 
            // enWarnMsg
            // 
            this.enWarnMsg.AutoSize = true;
            this.enWarnMsg.Location = new System.Drawing.Point(498, 115);
            this.enWarnMsg.Name = "enWarnMsg";
            this.enWarnMsg.Size = new System.Drawing.Size(199, 26);
            this.enWarnMsg.TabIndex = 12;
            this.enWarnMsg.Text = "Note:\r\n- Empty key will trigger no pre-encryption.";
            // 
            // enOptionInpt
            // 
            this.enOptionInpt.FormattingEnabled = true;
            this.enOptionInpt.Items.AddRange(new object[] {
            "1 bit Encryption",
            "2 bits Encryption"});
            this.enOptionInpt.Location = new System.Drawing.Point(148, 91);
            this.enOptionInpt.Name = "enOptionInpt";
            this.enOptionInpt.Size = new System.Drawing.Size(121, 21);
            this.enOptionInpt.TabIndex = 3;
            this.enOptionInpt.SelectedIndexChanged += new System.EventHandler(this.enModeChange);
            // 
            // enClearButt
            // 
            this.enClearButt.Location = new System.Drawing.Point(119, 144);
            this.enClearButt.Name = "enClearButt";
            this.enClearButt.Size = new System.Drawing.Size(75, 23);
            this.enClearButt.TabIndex = 5;
            this.enClearButt.Text = "Clear";
            this.enClearButt.UseVisualStyleBackColor = true;
            this.enClearButt.Click += new System.EventHandler(this.enClearButt_Click);
            // 
            // encryptButt
            // 
            this.encryptButt.Enabled = false;
            this.encryptButt.Location = new System.Drawing.Point(40, 144);
            this.encryptButt.Name = "encryptButt";
            this.encryptButt.Size = new System.Drawing.Size(75, 23);
            this.encryptButt.TabIndex = 4;
            this.encryptButt.Text = "Encrypt";
            this.encryptButt.UseVisualStyleBackColor = true;
            this.encryptButt.Click += new System.EventHandler(this.encryptButt_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(129, 91);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(13, 20);
            this.label3.TabIndex = 8;
            this.label3.Text = ":";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(129, 65);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(13, 20);
            this.label4.TabIndex = 8;
            this.label4.Text = ":";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(129, 37);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(13, 20);
            this.label5.TabIndex = 8;
            this.label5.Text = ":";
            // 
            // enKeyInpt
            // 
            this.enKeyInpt.Location = new System.Drawing.Point(148, 65);
            this.enKeyInpt.Name = "enKeyInpt";
            this.enKeyInpt.Size = new System.Drawing.Size(215, 20);
            this.enKeyInpt.TabIndex = 2;
            // 
            // enBrowseButt
            // 
            this.enBrowseButt.Location = new System.Drawing.Point(369, 38);
            this.enBrowseButt.Name = "enBrowseButt";
            this.enBrowseButt.Size = new System.Drawing.Size(75, 23);
            this.enBrowseButt.TabIndex = 1;
            this.enBrowseButt.Text = "Browse";
            this.enBrowseButt.UseVisualStyleBackColor = true;
            this.enBrowseButt.Click += new System.EventHandler(this.enBrowseButt_Click);
            // 
            // enFileInpt
            // 
            this.enFileInpt.Enabled = false;
            this.enFileInpt.Location = new System.Drawing.Point(148, 40);
            this.enFileInpt.Name = "enFileInpt";
            this.enFileInpt.Size = new System.Drawing.Size(215, 20);
            this.enFileInpt.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(36, 92);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 20);
            this.label6.TabIndex = 4;
            this.label6.Text = "Mode";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(36, 65);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 20);
            this.label7.TabIndex = 4;
            this.label7.Text = "Key";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(36, 38);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(73, 20);
            this.label8.TabIndex = 3;
            this.label8.Text = "Data File";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(5, 5);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(45, 18);
            this.label9.TabIndex = 1;
            this.label9.Text = "Input:";
            // 
            // enProgBar
            // 
            this.enProgBar.Location = new System.Drawing.Point(501, 150);
            this.enProgBar.Name = "enProgBar";
            this.enProgBar.Size = new System.Drawing.Size(214, 15);
            this.enProgBar.Step = 2;
            this.enProgBar.TabIndex = 0;
            // 
            // decryptionPage
            // 
            this.decryptionPage.Controls.Add(this.panel2);
            this.decryptionPage.Location = new System.Drawing.Point(4, 22);
            this.decryptionPage.Name = "decryptionPage";
            this.decryptionPage.Padding = new System.Windows.Forms.Padding(3);
            this.decryptionPage.Size = new System.Drawing.Size(732, 183);
            this.decryptionPage.TabIndex = 1;
            this.decryptionPage.Text = "Decryption";
            this.decryptionPage.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.deOriginal);
            this.panel2.Controls.Add(this.deWarnMsg);
            this.panel2.Controls.Add(this.deClearAll);
            this.panel2.Controls.Add(this.deOptionInpt);
            this.panel2.Controls.Add(this.deClearButt);
            this.panel2.Controls.Add(this.decryptButt);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.deKeyInpt);
            this.panel2.Controls.Add(this.deBrowseButt);
            this.panel2.Controls.Add(this.deFileInpt);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.deProgBar);
            this.panel2.Location = new System.Drawing.Point(6, 5);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(720, 173);
            this.panel2.TabIndex = 1;
            // 
            // deOriginal
            // 
            this.deOriginal.AutoSize = true;
            this.deOriginal.Location = new System.Drawing.Point(450, 42);
            this.deOriginal.Name = "deOriginal";
            this.deOriginal.Size = new System.Drawing.Size(144, 17);
            this.deOriginal.TabIndex = 19;
            this.deOriginal.Text = "Use the original Filename";
            this.deOriginal.UseVisualStyleBackColor = true;
            // 
            // deWarnMsg
            // 
            this.deWarnMsg.AutoSize = true;
            this.deWarnMsg.Location = new System.Drawing.Point(498, 115);
            this.deWarnMsg.Name = "deWarnMsg";
            this.deWarnMsg.Size = new System.Drawing.Size(204, 26);
            this.deWarnMsg.TabIndex = 15;
            this.deWarnMsg.Text = "Note:\r\n- Empty key will trigger no post-decryption.";
            // 
            // deClearAll
            // 
            this.deClearAll.Location = new System.Drawing.Point(200, 144);
            this.deClearAll.Name = "deClearAll";
            this.deClearAll.Size = new System.Drawing.Size(75, 23);
            this.deClearAll.TabIndex = 6;
            this.deClearAll.Text = "Clear All";
            this.deClearAll.UseVisualStyleBackColor = true;
            this.deClearAll.Click += new System.EventHandler(this.ClearAllButt_Click);
            // 
            // deOptionInpt
            // 
            this.deOptionInpt.FormattingEnabled = true;
            this.deOptionInpt.Items.AddRange(new object[] {
            "1 bit Encryption",
            "2 bits Encryption"});
            this.deOptionInpt.Location = new System.Drawing.Point(148, 91);
            this.deOptionInpt.Name = "deOptionInpt";
            this.deOptionInpt.Size = new System.Drawing.Size(121, 21);
            this.deOptionInpt.TabIndex = 3;
            this.deOptionInpt.SelectedIndexChanged += new System.EventHandler(this.deModeChange);
            // 
            // deClearButt
            // 
            this.deClearButt.Location = new System.Drawing.Point(119, 144);
            this.deClearButt.Name = "deClearButt";
            this.deClearButt.Size = new System.Drawing.Size(75, 23);
            this.deClearButt.TabIndex = 5;
            this.deClearButt.Text = "Clear";
            this.deClearButt.UseVisualStyleBackColor = true;
            this.deClearButt.Click += new System.EventHandler(this.deClearButt_Click);
            // 
            // decryptButt
            // 
            this.decryptButt.Enabled = false;
            this.decryptButt.Location = new System.Drawing.Point(40, 144);
            this.decryptButt.Name = "decryptButt";
            this.decryptButt.Size = new System.Drawing.Size(75, 23);
            this.decryptButt.TabIndex = 4;
            this.decryptButt.Text = "Decrypt";
            this.decryptButt.UseVisualStyleBackColor = true;
            this.decryptButt.Click += new System.EventHandler(this.decryptButt_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(129, 91);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(13, 20);
            this.label1.TabIndex = 8;
            this.label1.Text = ":";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(129, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(13, 20);
            this.label2.TabIndex = 8;
            this.label2.Text = ":";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(129, 37);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(13, 20);
            this.label10.TabIndex = 8;
            this.label10.Text = ":";
            // 
            // deKeyInpt
            // 
            this.deKeyInpt.Location = new System.Drawing.Point(148, 65);
            this.deKeyInpt.Name = "deKeyInpt";
            this.deKeyInpt.Size = new System.Drawing.Size(215, 20);
            this.deKeyInpt.TabIndex = 2;
            // 
            // deBrowseButt
            // 
            this.deBrowseButt.Location = new System.Drawing.Point(369, 38);
            this.deBrowseButt.Name = "deBrowseButt";
            this.deBrowseButt.Size = new System.Drawing.Size(75, 23);
            this.deBrowseButt.TabIndex = 1;
            this.deBrowseButt.Text = "Browse";
            this.deBrowseButt.UseVisualStyleBackColor = true;
            this.deBrowseButt.Click += new System.EventHandler(this.deBrowseButt_Click);
            // 
            // deFileInpt
            // 
            this.deFileInpt.Enabled = false;
            this.deFileInpt.Location = new System.Drawing.Point(148, 40);
            this.deFileInpt.Name = "deFileInpt";
            this.deFileInpt.Size = new System.Drawing.Size(215, 20);
            this.deFileInpt.TabIndex = 5;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(36, 92);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(49, 20);
            this.label11.TabIndex = 4;
            this.label11.Text = "Mode";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(36, 65);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(35, 20);
            this.label12.TabIndex = 4;
            this.label12.Text = "Key";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(36, 38);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(87, 20);
            this.label13.TabIndex = 3;
            this.label13.Text = "Output File";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(5, 5);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(56, 18);
            this.label14.TabIndex = 1;
            this.label14.Text = "Output:";
            // 
            // deProgBar
            // 
            this.deProgBar.Location = new System.Drawing.Point(501, 150);
            this.deProgBar.Name = "deProgBar";
            this.deProgBar.Size = new System.Drawing.Size(214, 15);
            this.deProgBar.Step = 2;
            this.deProgBar.TabIndex = 0;
            // 
            // helpPage
            // 
            this.helpPage.Controls.Add(this.splitContainer2);
            this.helpPage.Controls.Add(this.label17);
            this.helpPage.Location = new System.Drawing.Point(4, 22);
            this.helpPage.Name = "helpPage";
            this.helpPage.Size = new System.Drawing.Size(732, 183);
            this.helpPage.TabIndex = 3;
            this.helpPage.Text = "Help";
            this.helpPage.UseVisualStyleBackColor = true;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Location = new System.Drawing.Point(7, 24);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.label20);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.label21);
            this.splitContainer2.Size = new System.Drawing.Size(719, 156);
            this.splitContainer2.SplitterDistance = 351;
            this.splitContainer2.TabIndex = 2;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(9, 9);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(291, 130);
            this.label20.TabIndex = 2;
            this.label20.Text = resources.GetString("label20.Text");
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(9, 9);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(268, 130);
            this.label21.TabIndex = 1;
            this.label21.Text = resources.GetString("label21.Text");
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(4, 3);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(107, 18);
            this.label17.TabIndex = 1;
            this.label17.Text = "How To Use:";
            // 
            // aboutPage
            // 
            this.aboutPage.Controls.Add(this.label24);
            this.aboutPage.Controls.Add(this.label23);
            this.aboutPage.Controls.Add(this.label22);
            this.aboutPage.Controls.Add(this.label16);
            this.aboutPage.Controls.Add(this.label15);
            this.aboutPage.Location = new System.Drawing.Point(4, 22);
            this.aboutPage.Name = "aboutPage";
            this.aboutPage.Size = new System.Drawing.Size(732, 183);
            this.aboutPage.TabIndex = 2;
            this.aboutPage.Text = "About";
            this.aboutPage.UseVisualStyleBackColor = true;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(580, 161);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(146, 13);
            this.label24.TabIndex = 3;
            this.label24.Text = "February 2011 by XXX Team.";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(30, 117);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(361, 13);
            this.label23.TabIndex = 2;
            this.label23.Text = "This Software was made to accomplish Cryptography Team Assignment 01.";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(172, 40);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(71, 48);
            this.label22.TabIndex = 1;
            this.label22.Text = "/ 13507005\r\n/ 13507016\r\n/ 13507051";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(30, 40);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(136, 48);
            this.label16.TabIndex = 0;
            this.label16.Text = "1. Andika Pratama\r\n2. Haryus\r\n3. Moch. Yusup Soleh";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(12, 13);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(55, 18);
            this.label15.TabIndex = 0;
            this.label15.Text = "Team:";
            // 
            // openImage
            // 
            this.openImage.Filter = "Image Files|*.jpg;*.bmp;*.png;*.gif";
            // 
            // saveImage
            // 
            this.saveImage.Filter = "Bitmap Image|*.bmp|JPEG|*.jpg|PNG|*.png|GIF|*.gif";
            // 
            // splitContainer1
            // 
            this.splitContainer1.BackColor = System.Drawing.Color.White;
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Location = new System.Drawing.Point(12, 12);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.rawPreview);
            this.splitContainer1.Panel1.Controls.Add(this.label18);
            this.splitContainer1.Panel1.Controls.Add(this.rawSave);
            this.splitContainer1.Panel1.Controls.Add(this.rawPic);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.steganoPreview);
            this.splitContainer1.Panel2.Controls.Add(this.label19);
            this.splitContainer1.Panel2.Controls.Add(this.steganoSave);
            this.splitContainer1.Panel2.Controls.Add(this.steganoPic);
            this.splitContainer1.Size = new System.Drawing.Size(730, 287);
            this.splitContainer1.SplitterDistance = 363;
            this.splitContainer1.TabIndex = 1;
            // 
            // rawPreview
            // 
            this.rawPreview.AutoSize = true;
            this.rawPreview.BackColor = System.Drawing.Color.Transparent;
            this.rawPreview.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rawPreview.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.rawPreview.Location = new System.Drawing.Point(124, 109);
            this.rawPreview.Name = "rawPreview";
            this.rawPreview.Size = new System.Drawing.Size(114, 23);
            this.rawPreview.TabIndex = 3;
            this.rawPreview.Text = "[No Preview]";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label18.Location = new System.Drawing.Point(14, 257);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(131, 13);
            this.label18.TabIndex = 2;
            this.label18.Text = "Raw Image (Click To Add)";
            // 
            // rawSave
            // 
            this.rawSave.BackColor = System.Drawing.Color.Transparent;
            this.rawSave.Enabled = false;
            this.rawSave.Location = new System.Drawing.Point(303, 252);
            this.rawSave.Name = "rawSave";
            this.rawSave.Size = new System.Drawing.Size(42, 23);
            this.rawSave.TabIndex = 1;
            this.rawSave.Text = "Save";
            this.rawSave.UseVisualStyleBackColor = false;
            this.rawSave.Click += new System.EventHandler(this.rawSave_Click);
            // 
            // rawPic
            // 
            this.rawPic.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rawPic.Location = new System.Drawing.Point(10, 3);
            this.rawPic.Name = "rawPic";
            this.rawPic.Size = new System.Drawing.Size(345, 281);
            this.rawPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.rawPic.TabIndex = 0;
            this.rawPic.TabStop = false;
            this.rawPic.Click += new System.EventHandler(this.rawPic_Click);
            // 
            // steganoPreview
            // 
            this.steganoPreview.AutoSize = true;
            this.steganoPreview.BackColor = System.Drawing.Color.Transparent;
            this.steganoPreview.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.steganoPreview.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.steganoPreview.Location = new System.Drawing.Point(122, 109);
            this.steganoPreview.Name = "steganoPreview";
            this.steganoPreview.Size = new System.Drawing.Size(114, 23);
            this.steganoPreview.TabIndex = 4;
            this.steganoPreview.Text = "[No Preview]";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label19.Location = new System.Drawing.Point(12, 257);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(149, 13);
            this.label19.TabIndex = 3;
            this.label19.Text = "Stegano Image (Click To Add)";
            // 
            // steganoSave
            // 
            this.steganoSave.BackColor = System.Drawing.Color.Transparent;
            this.steganoSave.Enabled = false;
            this.steganoSave.Location = new System.Drawing.Point(302, 252);
            this.steganoSave.Name = "steganoSave";
            this.steganoSave.Size = new System.Drawing.Size(42, 23);
            this.steganoSave.TabIndex = 2;
            this.steganoSave.Text = "Save";
            this.steganoSave.UseVisualStyleBackColor = false;
            this.steganoSave.Click += new System.EventHandler(this.steganoSave_Click);
            // 
            // steganoPic
            // 
            this.steganoPic.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.steganoPic.Location = new System.Drawing.Point(8, 3);
            this.steganoPic.Name = "steganoPic";
            this.steganoPic.Size = new System.Drawing.Size(345, 281);
            this.steganoPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.steganoPic.TabIndex = 1;
            this.steganoPic.TabStop = false;
            this.steganoPic.Click += new System.EventHandler(this.steganoPic_Click);
            // 
            // openFile
            // 
            this.openFile.Filter = "All Files|*.*";
            // 
            // saveFile
            // 
            this.saveFile.Filter = "All Files|*.*";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(760, 526);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.tabCont);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "MainForm";
            this.Text = "Steganotastic";
            this.tabCont.ResumeLayout(false);
            this.encryptionPage.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.decryptionPage.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.helpPage.ResumeLayout(false);
            this.helpPage.PerformLayout();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.aboutPage.ResumeLayout(false);
            this.aboutPage.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rawPic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.steganoPic)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabCont;
        private System.Windows.Forms.TabPage decryptionPage;
        private System.Windows.Forms.TabPage aboutPage;
        private System.Windows.Forms.OpenFileDialog openImage;
        private System.Windows.Forms.SaveFileDialog saveImage;
        private System.Windows.Forms.TabPage encryptionPage;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox deOptionInpt;
        private System.Windows.Forms.Button deClearButt;
        private System.Windows.Forms.Button decryptButt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox deKeyInpt;
        private System.Windows.Forms.Button deBrowseButt;
        private System.Windows.Forms.TextBox deFileInpt;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ProgressBar deProgBar;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.OpenFileDialog openFile;
        private System.Windows.Forms.SaveFileDialog saveFile;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button psnrButt;
        private System.Windows.Forms.Button enClearAllButt;
        private System.Windows.Forms.Label enWarnMsg;
        private System.Windows.Forms.ComboBox enOptionInpt;
        private System.Windows.Forms.Button enClearButt;
        private System.Windows.Forms.Button encryptButt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox enKeyInpt;
        private System.Windows.Forms.Button enBrowseButt;
        private System.Windows.Forms.TextBox enFileInpt;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ProgressBar enProgBar;
        private System.Windows.Forms.Label deWarnMsg;
        private System.Windows.Forms.Button deClearAll;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button rawSave;
        private System.Windows.Forms.PictureBox rawPic;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button steganoSave;
        private System.Windows.Forms.PictureBox steganoPic;
        private System.Windows.Forms.Label rawPreview;
        private System.Windows.Forms.Label steganoPreview;
        private System.Windows.Forms.TabPage helpPage;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label enMaxSize;
        private System.Windows.Forms.Label enFileSize;
        private System.Windows.Forms.CheckBox deOriginal;
        private System.Windows.Forms.Label psnrTxt;

    }
}

