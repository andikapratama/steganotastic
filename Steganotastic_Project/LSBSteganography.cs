﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace Steganotastic
{
    class LSBSteganography
    {
        Bitmap bmp;
        byte[] data;
        int key;

        public LSBSteganography()
        {
            Bmp = null;
            Data = null;
            Key = 0;
        }

        public double MaxEncryption()
        {
            return (bmp.Size.Height * bmp.Size.Width * 3)/8;
        }

        public byte[] Data
        {
            get
            {
                return this.data;
            }
            set
            {
                this.data = value;
            }
        }

        public int Key
        {
            get
            {
                return this.key;
            }
            set
            {
                this.key = value;
            }
        }

        public void SetKey(String key)
        {
            Key = 1;
            foreach(char c in key)
            {
                Key += (int)c;
            }
        }

        public Bitmap Bmp
        {
            get
            {
                return this.bmp;
            }
            set
            {
                this.bmp = value;
            }
        }

        public LSBSteganography(Bitmap pBmp, byte[] pdata, int pkey)
        {
            Bmp = pBmp;
            Data = pdata;
            Key = pkey;
        }
        public Bitmap Encode(ProgressBar pBar)
        {
            Rectangle rect = new Rectangle(0, 0, bmp.Width, bmp.Height);
            System.Drawing.Imaging.BitmapData bmpData =
                bmp.LockBits(rect, System.Drawing.Imaging.ImageLockMode.ReadWrite,
                bmp.PixelFormat);
            // Get the address of the first line.
            IntPtr ptr = bmpData.Scan0;
            // Declare an array to hold the bytes of the bitmap.
            int bytes = Math.Abs(bmpData.Stride) * bmp.Height;
            byte[] rgbValues = new byte[bytes];
            long[] seq = Util.CreateEncodingSequence(rgbValues.LongLength, key);
            // Copy the RGB values into the array.
            System.Runtime.InteropServices.Marshal.Copy(ptr, rgbValues, 0, bytes);
            // Set every third value to 255. A 24bpp bitmap will look red.
            int i = 0;
            byte[] TotalDataLength = BitConverter.GetBytes(data.Length);
            pBar.Value = 20;
            foreach (byte b in TotalDataLength)
            {
                for (byte j = 0; j < 8; j++)
                {
                    rgbValues[seq[i]] = Util.SetBit(rgbValues[seq[i]], 0, Util.GetBit(b, j));
                    i++;
                }
            }
            pBar.Value = 60;
            foreach (byte b in data)
            {
                for (byte j = 0; j < 8; j++)
                {
                    rgbValues[seq[i]] = Util.SetBit(rgbValues[seq[i]], 0, Util.GetBit(b, j));
                    i++;
                }
            }
            pBar.Value = 80;
            // Copy the RGB values back to the bitmap
            System.Runtime.InteropServices.Marshal.Copy(rgbValues, 0, ptr, bytes);

            // Unlock the bits.
            bmp.UnlockBits(bmpData);
            pBar.Value = 85;
            return bmp;
        }

        public Bitmap Encode2bit(ProgressBar pBar)
        {
            Rectangle rect = new Rectangle(0, 0, bmp.Width, bmp.Height);
            System.Drawing.Imaging.BitmapData bmpData =
                bmp.LockBits(rect, System.Drawing.Imaging.ImageLockMode.ReadWrite,
                bmp.PixelFormat);
            // Get the address of the first line.
            IntPtr ptr = bmpData.Scan0;
            // Declare an array to hold the bytes of the bitmap.
            int bytes = Math.Abs(bmpData.Stride) * bmp.Height;
            byte[] rgbValues = new byte[bytes];
            long[] seq = Util.CreateEncodingSequence(rgbValues.LongLength, key);
            // Copy the RGB values into the array.
            System.Runtime.InteropServices.Marshal.Copy(ptr, rgbValues, 0, bytes);
            // Set every third value to 255. A 24bpp bitmap will look red.
            int i = 0;
            byte[] TotalDataLength = BitConverter.GetBytes(data.Length);
            pBar.Value = 20;
            foreach (byte b in TotalDataLength)
            {
                for (byte j = 0; j < 8; j++)
                {
                    rgbValues[seq[i]] = Util.SetBit(rgbValues[seq[i]], 0, Util.GetBit(b, j));
                    j++;
                    rgbValues[seq[i]] = Util.SetBit(rgbValues[seq[i]], 1, Util.GetBit(b, j));
                    i++;
                }
            }
            pBar.Value = 70;
            foreach (byte b in data)
            {
                for (byte j = 0; j < 8; j++)
                {
                    rgbValues[seq[i]] = Util.SetBit(rgbValues[seq[i]], 0, Util.GetBit(b, j));
                    j++;
                    rgbValues[seq[i]] = Util.SetBit(rgbValues[seq[i]], 1, Util.GetBit(b, j));
                    i++;
                }
            }
            pBar.Value = 85;
            // Copy the RGB values back to the bitmap
            System.Runtime.InteropServices.Marshal.Copy(rgbValues, 0, ptr, bytes);

            // Unlock the bits.
            bmp.UnlockBits(bmpData);
            pBar.Value = 90;
            return bmp;
        }

        public byte[] Decode(ProgressBar pBar)
        {
            Rectangle rect = new Rectangle(0, 0, bmp.Width, bmp.Height);
            System.Drawing.Imaging.BitmapData bmpData =
                bmp.LockBits(rect, System.Drawing.Imaging.ImageLockMode.ReadOnly,
                bmp.PixelFormat);
            // Get the address of the first line.
            IntPtr ptr = bmpData.Scan0;
            // Declare an array to hold the bytes of the bitmap.
            int bytes = Math.Abs(bmpData.Stride) * bmp.Height;
            byte[] rgbValues = new byte[bytes];
            long[] seq = Util.CreateEncodingSequence(rgbValues.LongLength, key);
            // Copy the RGB values into the array.
            System.Runtime.InteropServices.Marshal.Copy(ptr, rgbValues, 0, bytes);
            // Jumlah data harus dikodekan, kalu tidak, enggak ketahuan kapan harus berhenti
            byte[] TotalDataLengthByte = new byte[sizeof(int)];
            long i = 0;
            pBar.Value = 5;
            for (int k = 0; k < TotalDataLengthByte.Length; k++)
            {
                for (byte j = 0; j < 8; j++)
                    TotalDataLengthByte[k] = Util.SetBit(TotalDataLengthByte[k], j, Util.GetBit(rgbValues[seq[i++]], 0));
            }
            pBar.Value = 45;
            int TotalBDataLength = BitConverter.ToInt32(TotalDataLengthByte, 0);
            byte[] decodedData = new byte[TotalBDataLength];
            for (long k = 0; k < TotalBDataLength; k++)
            {
                for (byte j = 0; j < 8; j++)
                    decodedData[k] = Util.SetBit(decodedData[k], j, Util.GetBit(rgbValues[seq[i++]], 0));
            }
            pBar.Value = 75;
            // Unlock the bits.
            bmp.UnlockBits(bmpData);
            pBar.Value = 80;
            return decodedData;
        }

        public byte[] Decode2bit(ProgressBar pBar)
        {
            Rectangle rect = new Rectangle(0, 0, bmp.Width, bmp.Height);
            System.Drawing.Imaging.BitmapData bmpData =
                bmp.LockBits(rect, System.Drawing.Imaging.ImageLockMode.ReadOnly,
                bmp.PixelFormat);
            // Get the address of the first line.
            IntPtr ptr = bmpData.Scan0;
            // Declare an array to hold the bytes of the bitmap.
            int bytes = Math.Abs(bmpData.Stride) * bmp.Height;
            byte[] rgbValues = new byte[bytes];
            long[] seq = Util.CreateEncodingSequence(rgbValues.LongLength, key);
            // Copy the RGB values into the array.
            System.Runtime.InteropServices.Marshal.Copy(ptr, rgbValues, 0, bytes);
            // Jumlah data harus dikodekan, kalu tidak, enggak ketahuan kapan harus berhenti
            byte[] TotalDataLengthByte = new byte[sizeof(int)];
            long i = 0;
            pBar.Value = 5;
            for (int k = 0; k < TotalDataLengthByte.Length; k++)
            {
                for (byte j = 0; j < 8; j++)
                {
                    TotalDataLengthByte[k] = Util.SetBit(TotalDataLengthByte[k], j, Util.GetBit(rgbValues[seq[i]], 0));
                    j++;
                    TotalDataLengthByte[k] = Util.SetBit(TotalDataLengthByte[k], j, Util.GetBit(rgbValues[seq[i]], 1));
                    i++;
                }
            }
            pBar.Value = 55;
            int TotalBDataLength = BitConverter.ToInt32(TotalDataLengthByte, 0);
            byte[] decodedData = new byte[TotalBDataLength];
            for (long k = 0; k < TotalBDataLength; k++)
            {
                for (byte j = 0; j < 8; j++)
                {
                    decodedData[k] = Util.SetBit(decodedData[k], j, Util.GetBit(rgbValues[seq[i]], 0));
                    j++;
                    decodedData[k] = Util.SetBit(decodedData[k], j, Util.GetBit(rgbValues[seq[i]], 1));
                    i++;
                }
            }
            pBar.Value = 80;
            // Unlock the bits.
            bmp.UnlockBits(bmpData);
            pBar.Value = 85;
            return decodedData;
        }
    }
}
