﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Drawing;
using System.Windows.Forms;

namespace Steganotastic
{
    static class Util
    {

        public static long[] CreateEncodingSequence(long length, int key)
        {
            long[] seq = new long[length];
            for (int i = 0; i < seq.Length; i++)
                seq[i] = i;
            seq = Array.FindAll(seq, a => (a + 1) % 4 != 0);
            return Util.Shuffle(seq, key);
        }

        public static bool GetBit(byte b, byte position)
        {
            return ((b & (byte)(1 << position)) != 0);
        }

        public static byte SetBit(byte b, byte position, bool newBitValue)
        {
            byte mask = (byte)(1 << position);
            if (newBitValue)
            {
                return (byte)(b | mask);
            }
            else
            {
                return (byte)(b & ~mask);
            }
        }

        public static byte[] ToByteArray(this BitArray bits)
        {
            int numBytes = bits.Count / 8;
            if (bits.Count % 8 != 0) numBytes++;

            byte[] bytes = new byte[numBytes];
            int byteIndex = 0, bitIndex = 0;

            for (int i = 0; i < bits.Count; i++)
            {
                if (bits[i])
                    bytes[byteIndex] |= (byte)(1 << (7 - bitIndex));

                bitIndex++;
                if (bitIndex == 8)
                {
                    bitIndex = 0;
                    byteIndex++;
                }
            }

            return bytes;
        }

        public static int[] GetShuffleExchanges(int size, int key)
        {
            int[] exchanges = new int[size - 1];
            var rand = new Random(key);
            for (int i = size - 1; i > 0; i--)
            {
                int n = rand.Next(i + 1);
                exchanges[size - 1 - i] = n;
            }
            return exchanges;
        }

        public static int[] Shuffle(int[] toShuffle, int key)
        {
            int size = toShuffle.Length;
            var exchanges = GetShuffleExchanges(size, key);
            for (int i = size - 1; i > 0; i--)
            {
                int n = exchanges[size - 1 - i];
                int tmp = toShuffle[i];
                toShuffle[i] = toShuffle[n];
                toShuffle[n] = tmp;
            }
            return toShuffle;
        }

        public static long[] Shuffle(long[] toShuffle, int key)
        {
            int size = toShuffle.Length;
            var exchanges = GetShuffleExchanges(size, key);
            for (int i = size - 1; i > 0; i--)
            {
                int n = exchanges[size - 1 - i];
                long tmp = toShuffle[i];
                toShuffle[i] = toShuffle[n];
                toShuffle[n] = tmp;
            }
            return toShuffle;
        }

        public static int[] DeShuffle(int[] shuffled, int key)
        {
            int size = shuffled.Length;
            var exchanges = GetShuffleExchanges(size, key);
            for (int i = 1; i < size; i++)
            {
                int n = exchanges[size - i - 1];
                int tmp = shuffled[i];
                shuffled[i] = shuffled[n];
                shuffled[n] = tmp;
            }
            return shuffled;
        }

        /// <summary>
        /// Mengubah dari file menjadi array byte yang siap di steganokan
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static byte[] GetBytesFromFile(string path)
        {
            byte[] file = File.ReadAllBytes(path);
            char[] delim = { '\\' };
            string[] filenam = path.Split(delim, StringSplitOptions.RemoveEmptyEntries);
            byte[] filename = UnicodeEncoding.Unicode.GetBytes(filenam[filenam.Length - 1]);
            byte[] length = BitConverter.GetBytes(filename.Length);
            byte[] total = new byte[file.LongLength + filename.LongLength + length.LongLength];
            length.CopyTo(total, 0);
            filename.CopyTo(total, length.Length);
            file.CopyTo(total, length.Length + filename.Length);
            return total;
        }


        public static void GetFileFromBytes(byte[] bytes, out byte[] filedata, out string filename)
        {
            MemoryStream ms = new MemoryStream(bytes);
            BinaryReader br = new BinaryReader(ms);
            int length = br.ReadInt32();
            filename = UnicodeEncoding.Unicode.GetString(br.ReadBytes(length));
            filedata = br.ReadBytes((int)(br.BaseStream.Length - br.BaseStream.Position));
        }

        public static void GetFileFromBytesA(byte[] bytes, out byte[] filedata, out string filename)
        {
            byte[] lengthName = new byte[sizeof(int)];
            int i = 0;
            for (; i < sizeof(int); i++)
            {
                lengthName[i] = bytes[i];
            }
            int LengthName = BitConverter.ToInt32(lengthName, 0);
            byte[] fileName = new byte[LengthName];
            for (int j = 0; i < LengthName + sizeof(int); i++, j++)
            {
                fileName[j] = bytes[i];
            }
            filename = UnicodeEncoding.Unicode.GetString(fileName);
            byte[] fileData = new byte[bytes.Length - (LengthName + sizeof(int))];
            for (int j = 0; j < fileData.Length; i++, j++)
            {
                fileData[j] = bytes[i];
            }
            filedata = fileData;
        }

        public static bool writeByteArrayToFile(byte[] buff, string fileName)
        {
            bool response = false;

            try
            {
                FileStream fs = new FileStream(fileName, FileMode.Create, FileAccess.ReadWrite);
                BinaryWriter bw = new BinaryWriter(fs);
                bw.Write(buff);
                bw.Close();
                response = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return response;
        }

        static int mod(int a, int b)
        {
            if (a < 0)
                return b + (a % b);
            else
                return a % b;
        }


        public static byte[] VigenereEncode(byte[] data, string key)
        {
            byte[] encrypted = new byte[data.Length];
            int j = 0;
            for (int i = 0; i < data.Length; i++)
            {
                encrypted[i] = (byte)mod((data[i] + key[j]), 256);
                if (j == key.Length - 1)
                {
                    j = 0;
                }
                else
                {
                    j++;
                }
            }
            return encrypted;
        }

        public static byte[] VigenereDecode(byte[] data, string key)
        {
            byte[] encrypted = new byte[data.Length];
            int j = 0;
            for (int i = 0; i < data.Length; i++)
            {
                encrypted[i] = (byte)mod((data[i] - key[j]), 256);
                if (j == key.Length - 1)
                {
                    j = 0;
                }
                else
                {
                    j++;
                }
            }
            return encrypted;
        }

        /// <summary>
        /// calculating and get PSNR value.
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <returns></returns>
        public static double GetPSNR(Bitmap p1, Bitmap p2, ProgressBar pBar)
        {
            //pBar.Value = 30;
            double psnr = 0;
            double rms = 0;            

            if (p1.Height != p2.Height || p1.Width != p2.Width)
                return -1;
            int h = p1.Height;
            int w = p2.Width;
            
            pBar.Value = 20;
            Rectangle rect = new Rectangle(0, 0, w, h);
            System.Drawing.Imaging.BitmapData picData1 = p1.LockBits(rect, System.Drawing.Imaging.ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            System.Drawing.Imaging.BitmapData picData2 = p2.LockBits(rect, System.Drawing.Imaging.ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            IntPtr ptr1 = picData1.Scan0;
            IntPtr ptr2 = picData2.Scan0;
            int byteCount = w*h;
            byte[] rgbValue1 = new byte[byteCount];
            byte[] rgbValue2 = new byte[byteCount];
            System.Runtime.InteropServices.Marshal.Copy(ptr1, rgbValue1, 0, byteCount);
            System.Runtime.InteropServices.Marshal.Copy(ptr2, rgbValue2, 0, byteCount);

            pBar.Value = 30;
            double sum = 0;
            for (int i = 0; i < byteCount; ++i)
                sum += Math.Pow(rgbValue1[i] - rgbValue2[i], 2);

            pBar.Value = 80;
            System.Runtime.InteropServices.Marshal.Copy(rgbValue1, 0, ptr1, byteCount);
            System.Runtime.InteropServices.Marshal.Copy(rgbValue2, 0, ptr2, byteCount);
            p1.UnlockBits(picData1);
            p2.UnlockBits(picData2);

            rms = Math.Sqrt(sum / byteCount);
            psnr = 20 * Math.Log10(256 / rms);
            pBar.Value = 90;
            return psnr;
        }

        public static String GetDirectory(String filepath)
        {
            String dir = "";
            char[] delimit = { '\\' };
            string[] temp = filepath.Split(delimit, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < temp.Length - 1; ++i)
            {
                dir += temp[i] + "\\";
            }

            return dir;
        }

        public static String GetFiletype(String filename)
        {
            String ext = "";

            char[] delimit = { '.' };
            string[] temp = filename.Split(delimit, StringSplitOptions.RemoveEmptyEntries);
            ext = "."+temp[temp.Length - 1];

            return ext;
        }
    }
}
