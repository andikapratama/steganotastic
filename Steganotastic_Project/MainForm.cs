﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Steganotastic
{
    public partial class MainForm : Form
    {
        private LSBSteganography raw;
        private LSBSteganography steg;
        public MainForm()
        {
            raw = new LSBSteganography();
            steg = new LSBSteganography();
            InitializeComponent();
            enOptionInpt.SelectedIndex = 0;
            deOptionInpt.SelectedIndex = 0;
        }

        private void rawPic_Click(object sender, EventArgs e)
        {
            if (openImage.ShowDialog() == DialogResult.OK)
            {
                rawPic.ImageLocation = openImage.FileName;
                raw.Bmp = new Bitmap(Image.FromFile(@openImage.FileName));
                rawPreview.Visible = false;
                rawSave.Enabled = true;
                rawPreview.Enabled = false;
                enMaxSize.Text = String.Format("{0} KB",raw.MaxEncryption()/(1024));
                if (steg.Bmp != null)
                    psnrButt.Enabled = true;
                if (enFileInpt.Text != null && enFileInpt.Text != "")
                    encryptButt.Enabled = true;
            }
        }

        private void steganoPic_Click(object sender, EventArgs e)
        {
            if (openImage.ShowDialog() == DialogResult.OK)
            {
                steganoPic.ImageLocation = openImage.FileName;
                steg.Bmp = new Bitmap(Image.FromFile(@openImage.FileName));
                steganoPreview.Visible = false;
                steganoSave.Enabled = true;
                steganoPreview.Enabled = false;
                if (raw.Bmp != null)
                    psnrButt.Enabled = true;
                if (deFileInpt.Text != null && deFileInpt.Text != "")
                    decryptButt.Enabled = true;
            }
        }

        private void rawSave_Click(object sender, EventArgs e)
        {
            if (saveImage.ShowDialog() == DialogResult.OK)
            {
                switch (saveImage.FilterIndex)
                {
                    case 2:
                        raw.Bmp.Save(@saveImage.FileName, System.Drawing.Imaging.ImageFormat.Jpeg);
                        break;
                    case 3:
                        raw.Bmp.Save(@saveImage.FileName, System.Drawing.Imaging.ImageFormat.Png);
                        break;
                    case 4:
                        raw.Bmp.Save(@saveImage.FileName, System.Drawing.Imaging.ImageFormat.Gif);
                        break;
                    default:
                        raw.Bmp.Save(@saveImage.FileName, System.Drawing.Imaging.ImageFormat.Bmp);
                        break;
                }            
            }
        }

        private void steganoSave_Click(object sender, EventArgs e)
        {
            if (saveImage.ShowDialog() == DialogResult.OK)
            {
                switch (saveImage.FilterIndex)
                {
                    case 2:
                        steg.Bmp.Save(@saveImage.FileName, System.Drawing.Imaging.ImageFormat.Jpeg);
                        break;
                    case 3:
                        steg.Bmp.Save(@saveImage.FileName, System.Drawing.Imaging.ImageFormat.Png);
                        break;
                    case 4:
                        steg.Bmp.Save(@saveImage.FileName, System.Drawing.Imaging.ImageFormat.Gif);
                        break;
                    default:
                        steg.Bmp.Save(@saveImage.FileName, System.Drawing.Imaging.ImageFormat.Bmp);
                        break;
                }
            }
        }

        private void enClearButt_Click(object sender, EventArgs e)
        {
            enClear();
        }

        private void deClearButt_Click(object sender, EventArgs e)
        {
            deClear();
        }

        private void ClearAllButt_Click(object sender, EventArgs e)
        {
            enClear();
            deClear();
            raw.Bmp = null;
            raw.Data = null;
            rawPic.Image = null;
            rawPic.ImageLocation = "";
            rawPreview.Visible = true;
            steg.Bmp = null;
            steganoPic.Image = null;
            steganoPic.ImageLocation = "";
            steganoPreview.Visible = true;
            enMaxSize.Text = "0 KB";
            encryptButt.Enabled = false;
            decryptButt.Enabled = false;
            psnrButt.Enabled = false;
            psnrTxt.Text = "0 DB";
        }

        private void enClear()
        {
            raw.Data = null;
            enFileInpt.Text = "";
            enKeyInpt.Text = "";
            enFileSize.Text = "0 KB";
            enProgBar.Value = 0;
        }

        private void deClear()
        {
            deFileInpt.Text = "";
            deKeyInpt.Text = "";
            deProgBar.Value = 0;
        }

        private void enBrowseButt_Click(object sender, EventArgs e)
        {
            if (openFile.ShowDialog() == DialogResult.OK)
            {
                enFileInpt.Text = openFile.FileName;
                byte[] file = Util.GetBytesFromFile(openFile.FileName);
                enFileSize.Text = String.Format("{0} KB", file.Length / 1024);
                if (raw.Bmp != null)
                    encryptButt.Enabled = true;
            }
        }
 
        private void deBrowseButt_Click(object sender, EventArgs e)
        {
            if (saveFile.ShowDialog() == DialogResult.OK)
            {
                deFileInpt.Text = saveFile.FileName;
                if (steg.Bmp != null)
                    decryptButt.Enabled = true;
            }
        }

        private void psnrButt_Click(object sender, EventArgs e)
        {
            enProgBar.Value = 0;
            psnrTxt.Text = "";
            double psnr = Util.GetPSNR(raw.Bmp, steg.Bmp, enProgBar);
            if (psnr == -1)
                MessageBox.Show("Make sure both picture are the on the same size.", "Pictures didn't match!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            else
            {
                psnrTxt.Text = String.Format("= {0} DB", psnr);
                enProgBar.Value = 100;
            }
        }

        private void enModeChange(object sender, EventArgs e)
        {
            deOptionInpt.SelectedIndex = enOptionInpt.SelectedIndex;
            if(raw.Bmp != null)
                enMaxSize.Text = String.Format("{0} KB",(raw.MaxEncryption()/1024)*(enOptionInpt.SelectedIndex+1));
        }

        private void deModeChange(object sender, EventArgs e)
        {
            enOptionInpt.SelectedIndex = deOptionInpt.SelectedIndex;
        }

        private void encryptButt_Click(object sender, EventArgs e)
        {
            String errMsg = "";
            byte[] fileStegano = Util.GetBytesFromFile(@enFileInpt.Text);
            if (fileStegano.Length > (raw.MaxEncryption()*(enOptionInpt.SelectedIndex+1)))
                errMsg = "The size of data is larger.\nMake sure the size of data is less than the Max Size.";
            if (errMsg == "")
            {
                enProgBar.Value = 0;
                steg.Bmp = new Bitmap(raw.Bmp);
                if (enKeyInpt.Text != "" && enKeyInpt.Text != null)
                    fileStegano = Util.VigenereEncode(fileStegano, enKeyInpt.Text);
                steg.Data = fileStegano;
                steg.SetKey(enKeyInpt.Text);
                if (enOptionInpt.SelectedIndex == 0)
                    steg.Encode(enProgBar);
                else
                    steg.Encode2bit(enProgBar);
                enProgBar.Value = 100;
                System.Threading.Thread.Sleep(300);
                MessageBox.Show("Encryption Successful.!\nPlease use \"save\" button to save image.", "Process completed.",MessageBoxButtons.OK,MessageBoxIcon.Information);
                steganoPic.Image = steg.Bmp;
                steganoPreview.Visible = false;
                rawPreview.Visible = false;
                psnrButt.Enabled = true;
                psnrTxt.Text = "= 0 DB";
                steganoSave.Enabled = true;
                decryptButt.Enabled = true;
            }
            else
                MessageBox.Show(errMsg);
        }

        private void decryptButt_Click(object sender, EventArgs e)
        {
            try
            {
                deProgBar.Value = 0;
                steg.SetKey(deKeyInpt.Text);
                byte[] hasil = deOptionInpt.SelectedIndex == 0 ? steg.Decode(deProgBar) : steg.Decode2bit(deProgBar);
                if (deKeyInpt.Text != "" && deKeyInpt.Text != null)
                    hasil = Util.VigenereDecode(hasil, deKeyInpt.Text);
                byte[] filedata;
                string filename = string.Empty;
                Util.GetFileFromBytesA(hasil, out filedata, out filename);
                deProgBar.Value = 100;
                if (!deOriginal.Checked)
                    filename = deFileInpt.Text + Util.GetFiletype(filename);
                else
                    filename = Util.GetDirectory(deFileInpt.Text) + filename;
                //MessageBox.Show(filename + " | " + Util.GetDirectory(deFileInpt.Text));
                Util.writeByteArrayToFile(filedata, filename);
                System.Threading.Thread.Sleep(200);
                MessageBox.Show("Decryption Successful.!\nFile has been saved on:\n"+filename, "Process completed", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ee)
            {
                MessageBox.Show("Cannot Decrypt the Picture!\nIt may be caused by wrong key or mode.\nPlease check again the key or mode decryption.", "Error Decryption!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}
